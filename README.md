This is fifth version of MVP architecture project, which shows how to work with RX.

App was made at learning course [http://skill-branch.ru](http://skill-branch.ru)


**Features**:

- imitation of catalog load and user enter

- validation of user fields and different types of animation

- support of orientation change

- added catalog screen via fragments

- added dots indicator, goods counter

- added check of "Back" button press in fragments