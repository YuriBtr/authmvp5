package yuribtr.skillbranch.ru.mvpauth5.di.components;

import android.content.Context;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
