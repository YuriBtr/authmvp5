package yuribtr.skillbranch.ru.mvpauth5.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.ICatalogView;

public class CatalogView extends RelativeLayout implements ICatalogView {

    @BindView(R.id.add_to_card_btn)
    Button mAddToCartBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        if (!isInEditMode()) {
            //showViewFromState();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter();
        for (ProductDto product : productsList) {
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
        //mCircleIndicator.setViewPager(mProductPager);
    }

    @Override
    public void updateProductCounter() {
        //todo:implement this
        //RootActivity rootActivity = (RootActivity) getActivity();
        //rootActivity.updateCartQuantity();
        //getRootActivity().updateCartQuantity();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @OnClick(R.id.add_to_card_btn)
    void clickAddToCard() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }
}
