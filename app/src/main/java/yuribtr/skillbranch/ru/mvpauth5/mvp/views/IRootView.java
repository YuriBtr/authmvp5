package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

import android.support.annotation.Nullable;

import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserInfoDto;

public interface IRootView extends IView {
    void showMessage(String message);

    void showMessage(int messageId);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();

    void setMenuItemChecked(String name);

    @Nullable
    IView getCurrentScreen();

    void initDrawer(UserInfoDto userInfoDto);
}
