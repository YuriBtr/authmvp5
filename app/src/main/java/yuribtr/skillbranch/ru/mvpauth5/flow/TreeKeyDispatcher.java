package yuribtr.skillbranch.ru.mvpauth5.flow;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import java.util.Collections;
import java.util.Map;

import flow.Direction;
import flow.Dispatcher;
import flow.KeyChanger;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.mortar.ScreenScoper;

//this class returns MortarScope for requested Screen
public class TreeKeyDispatcher extends KeyChanger implements Dispatcher {
    private Activity mActivity;
    private Object inKey;//valueobject, screen which will be showed
    @Nullable
    private Object outKey;//valueobject, screen which will be hided
    private FrameLayout mRootFrame;

    public TreeKeyDispatcher(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void dispatch(Traversal traversal, TraversalCallback callback) {
        Map<Object, Context> contexts;
        State inState = traversal.getState(traversal.destination.top());
        inKey = inState.getKey();
        //traversal.origin might be null, if we load first screen
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        outKey = outState == null ? null : outState.getKey();

        mRootFrame = (FrameLayout) mActivity.findViewById(R.id.root_frame);

        if (inKey.equals(outKey)) {//if we click on Drawer menu twice, keys.equals and gethash need to be overrided
            callback.onTraversalCompleted();
            return;
        }

        if (inKey instanceof TreeKey) {
            //todo: implement treekey navigation (ScreenScoper refactor) 01:11:10
        }

        Context flowContext = traversal.createContext(inKey, mActivity);//transfer screen and base Context
        Context mortarContext = ScreenScoper.getScreenScope((AbstractScreen) inKey).createContext(flowContext);
        contexts = Collections.singletonMap(inKey, mortarContext);
        changeKey(outState, inState, traversal.direction, contexts, callback);
    }

    @Override
    public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction, Map<Object, Context> incomingContexts, TraversalCallback callback) {
        Context context = incomingContexts.get(inKey);

        //saving previous screen/view, which are on the top of frame container
        if (outgoingState != null) {
            outgoingState.save(mRootFrame.getChildAt(0));
        }

        //create new view
        Screen screen;
        screen = inKey.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " + ((AbstractScreen) inKey).getScopeName());
        } else {
            int layout = screen.value();

            LayoutInflater inflater = LayoutInflater.from(context);//this inflater has special Flow content
            View newView = inflater.inflate(layout, mRootFrame, false);

            incomingState.restore(newView);//restore state to new View

            //todo: unregister screen scope and delete old view
            //prevents destroy scope when switching to AddAddressScreen
            if (outKey != null && !(inKey instanceof TreeKey)) {
                ((AbstractScreen) outKey).unRegisterScope();
            }

            if (mRootFrame.getChildAt(0) != null) {//delete old view
                mRootFrame.removeView(mRootFrame.getChildAt(0));
            }

            mRootFrame.addView(newView);
            callback.onTraversalCompleted();//says that transition completed
        }
    }
}
