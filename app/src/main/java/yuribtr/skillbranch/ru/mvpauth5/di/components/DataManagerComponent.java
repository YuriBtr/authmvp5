package yuribtr.skillbranch.ru.mvpauth5.di.components;

import javax.inject.Singleton;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.LocalModule;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.NetworkModule;

//наследуемся от AppComponent чтобы получить Context для LocalModule
@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
