package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

import android.net.Uri;

public interface IImageChooseNotifier {
    void onImageChoose(Uri uri);
}
