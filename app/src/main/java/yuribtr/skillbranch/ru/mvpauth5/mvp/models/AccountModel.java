package yuribtr.skillbranch.ru.mvpauth5.mvp.models;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserInfoDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserSettingsDto;

import static yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager.PROFILE_AVATAR_KEY;
import static yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager.PROFILE_FULL_NAME_KEY;
import static yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager.PROFILE_PHONE_KEY;

public class AccountModel extends AbstractModel {

    private BehaviorSubject<UserInfoDto> mUserInfoObs = BehaviorSubject.create();

    public AccountModel() {
        mUserInfoObs.onNext(getUserProfileInfo());
    }

    //region================UserProfile================

    public void saveProfileInfo(UserInfoDto userInfo) {
        mDataManager.saveProfileInfo(userInfo.getName(), userInfo.getPhone(), userInfo.getAvatar());
        mUserInfoObs.onNext(userInfo);
    }

    private UserInfoDto getUserProfileInfo() {
        Map<String, String> map = mDataManager.getUserProfileInfo();
        return new UserInfoDto(map.get(PROFILE_FULL_NAME_KEY),
                map.get(PROFILE_PHONE_KEY),
                map.get(PROFILE_AVATAR_KEY));
    }

    public Observable<UserInfoDto> getUserInfoObs() {
        return mUserInfoObs;
    }

    //endregion

    //region================Addresses================
    public Observable<UserAddressDto> getAddressObs() {
        return Observable.from(getUserAddresses());
    }

    private ArrayList<UserAddressDto> getUserAddresses() {
        return mDataManager.getUserAddress();
    }

    public void removeAddress(UserAddressDto userAddressDto) {
        mDataManager.removeUserAddress(userAddressDto.getId());
    }

    public UserAddressDto getAddressFromPosition(int position) {
        return getUserAddresses().get(position);
    }

    public void updateOrInsertAddress(UserAddressDto userAddressDto) {
        mDataManager.updateOrInsertAddress(userAddressDto);
    }

    //endregion

    //region================Settings================

    public Observable<UserSettingsDto> getUserSettingsObs() {
        return Observable.just(getUserSettings());
    }

    private UserSettingsDto getUserSettings() {
        Map<String, Boolean> map = mDataManager.getUserSettings();
        return new UserSettingsDto(map.get(ConstantManager.NOTIFICATION_ORDER_KEY),
                map.get(ConstantManager.NOTIFICATION_PROMO_KEY));
    }

    public void saveSettings(UserSettingsDto settings) {
        mDataManager.saveSetting(ConstantManager.NOTIFICATION_ORDER_KEY, settings.isOrderNotification());
        mDataManager.saveSetting(ConstantManager.NOTIFICATION_PROMO_KEY, settings.isPromoNotification());
    }
    //endregion
}
