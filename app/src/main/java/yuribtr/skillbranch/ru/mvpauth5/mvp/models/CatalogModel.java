package yuribtr.skillbranch.ru.mvpauth5.mvp.models;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;

public class CatalogModel extends AbstractModel {

    public List<ProductDto> getProductList() {
        return mDataManager.getMockProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }

    public ProductDto getProductById(int productId) {
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
