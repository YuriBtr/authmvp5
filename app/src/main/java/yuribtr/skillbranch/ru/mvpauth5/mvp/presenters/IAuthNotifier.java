package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

public interface IAuthNotifier {

    void onLoginSuccess();

    void onLoginError(String message);
}
