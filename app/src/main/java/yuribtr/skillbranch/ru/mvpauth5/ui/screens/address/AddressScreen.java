package yuribtr.skillbranch.ru.mvpauth5.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.AddressScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth5.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.IAddressPresenter;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.account.AccountScreen;

@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {
    @Nullable
    private UserAddressDto mAddressDto;

    public AddressScreen(@Nullable UserAddressDto addressDto) {
        mAddressDto = addressDto;
    }

    //below method overrided for FLOW (if keys are identical, FLOW will not change screens and do nothing)
    @Override
    public boolean equals(Object o) {
        if (mAddressDto != null) {
            return o instanceof AddressScreen && mAddressDto.equals(((AddressScreen) o).mAddressDto);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return mAddressDto != null ? mAddressDto.hashCode() : super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder().component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }
    //endregion

    //region================Presenter================
    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mAddressDto != null && getView() != null) {
                getView().initView(mAddressDto);
            }
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() != null) {
                UserAddressDto userAddressDto = getView().getUserAddress();
                if (userAddressDto.getName().isEmpty() || userAddressDto.getStreet().isEmpty()) {
                    getView().showInputError();
                } else {
                    getView().showMessage(R.string.address_added);
                    mAccountModel.updateOrInsertAddress(userAddressDto);
                    Flow.get(getView()).goBack();
                }
            }
        }
    }
    //endregion


}
