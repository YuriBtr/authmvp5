package yuribtr.skillbranch.ru.mvpauth5.utils;

public class AppConfig {
    private AppConfig() {
    }

    public static final String BASE_URL = "http://someurl.com";
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
