package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import javax.inject.Inject;

import mortar.MortarScope;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import yuribtr.skillbranch.ru.mvpauth5.App;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ActivityResultDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserInfoDto;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.RootActivity;

public class RootPresenter extends AbstractPresenter<IRootView> {
    private BehaviorSubject<ActivityResultDto> mActivityResultDtoObs = BehaviorSubject.create();
    private PublishSubject<Integer> mPermissionResultObs = PublishSubject.create();
    @Inject
    AccountModel mAccountModel;


    public RootPresenter() {
        //make access to account info on every screen
        App.getRootActivityRootComponent().inject(this);
    }

    public PublishSubject<Integer> getPermissionResultObs() {
        return mPermissionResultObs;
    }

    public BehaviorSubject<ActivityResultDto> getActivityResultDtoObs() {
        return mActivityResultDtoObs;
    }

    @Override
    public void initView(MortarScope scope) {
        if (getView() != null) {
            getView().setMenuItemChecked(scope.getName());
        }
        mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                //do not use inner anonymous class with FRODO logging
                .subscribe(new UserInfoSubscriber());
    }

    //making separate class for usage FRODO logging
    @RxLogSubscriber
    private class UserInfoSubscriber extends Subscriber<UserInfoDto> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(UserInfoDto userInfoDto) {
            if (getView() != null) {
                getView().initDrawer(userInfoDto);
            }
        }
    }

    public boolean checkPermissionAndRequestIfNotGranted(@NonNull String[] permissions, int requestCode) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission((RootActivity) getView(), permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
            return false;
        }
        return allGranted;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mActivityResultDtoObs.onNext(new ActivityResultDto(requestCode, resultCode, data));
    }

    public void onRequestPermissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResult) {
        if (getView() != null) {
            boolean allGranted = true;
            for (int i = 0; i < grantResult.length; i++) {
                if (grantResult[i] == PackageManager.PERMISSION_DENIED) {
                    allGranted = false;
                    switch (requestCode) {
                        case (ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE):
                            getView().showMessage(R.string.not_enough_permissions_for_gallery);
                            break;
                        case (ConstantManager.REQUEST_PERMISSION_CAMERA):
                            getView().showMessage(R.string.not_enough_permissions_for_camera);
                            break;
                    }
                    break;
                }
            }
            if (allGranted) {
                mPermissionResultObs.onNext(requestCode);
            }
        }
    }
}
