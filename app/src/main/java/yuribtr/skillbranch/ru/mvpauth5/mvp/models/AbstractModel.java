package yuribtr.skillbranch.ru.mvpauth5.mvp.models;

import javax.inject.Inject;

import yuribtr.skillbranch.ru.mvpauth5.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.components.DaggerModelComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.components.ModelComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.ModelModule;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
        //todo: 01:15:40 optimize createDaggerComponent
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
