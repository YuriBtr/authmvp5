package yuribtr.skillbranch.ru.mvpauth5;

import android.app.Application;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.components.AppComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.components.DaggerAppComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.AppModule;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.PicassoCacheModule;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.RootModule;
import yuribtr.skillbranch.ru.mvpauth5.mortar.ScreenScoper;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.DaggerRootActivity_RootComponent;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth5.utils.ComponentReflectionInjector;
import yuribtr.skillbranch.ru.mvpauth5.utils.Injector;

public class App extends Application implements Injector {
    private ComponentReflectionInjector<AppComponent> injector;
    private static AppComponent sAppComponent;
    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private static RootActivity.RootComponent mRootActivityRootComponent;

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createAppComponent();
        createRootActivityComponent();

        //here we create main root scope
        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())//BundleServiceRunner will save presenters in bundles
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }

    @Override
    public void inject(Object target) {
        injector.inject(target);
    }

    public static RootActivity.RootComponent getRootActivityRootComponent() {
        return mRootActivityRootComponent;
    }
}
