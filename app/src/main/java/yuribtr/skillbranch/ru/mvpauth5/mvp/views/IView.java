package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

public interface IView {
    boolean viewOnBackPressed();
}
