package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);

    boolean checkUserAuth();
}
