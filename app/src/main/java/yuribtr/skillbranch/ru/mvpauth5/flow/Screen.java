package yuribtr.skillbranch.ru.mvpauth5.flow;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Scope;

@Scope//здесь будем получать идентификатор XML ресурса - лэйаута
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Screen {//keyobject, valueobject, screen - одно и тоже

    int value();
}
