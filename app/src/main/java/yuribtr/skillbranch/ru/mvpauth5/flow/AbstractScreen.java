package yuribtr.skillbranch.ru.mvpauth5.flow;

import android.util.Log;

import flow.ClassKey;
import yuribtr.skillbranch.ru.mvpauth5.mortar.ScreenScoper;

public abstract class AbstractScreen<T> extends ClassKey {
    private static final String TAG = "AbstractScreen";

    //todo: when screens are same type but receives different objects in constructor, getScopeName and unRegisterScope should be overrrided
    public String getScopeName() {
        return getClass().getName();
    }

    //creating scopes with names = class name
    public abstract Object createScreenComponent(T parentComponent);

    //destroy scope after screen exit
    public void unRegisterScope() {
        Log.e(TAG, "unRegisterScope: " + this.getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }

}
