package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

public interface IProductPresenter {
    void clickOnPlus();

    void clickOnMinus();
}
