package yuribtr.skillbranch.ru.mvpauth5.data.managers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class PreferencesManager {
    private String packageName;
    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = getDefaultSharedPreferences(context);
        packageName = context.getPackageName();
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN, authToken);
        editor.apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, null);
    }

    public void saveUserProfileInfo(String name, String phone, String avatar) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.PROFILE_FULL_NAME_KEY, name);
        editor.putString(ConstantManager.PROFILE_PHONE_KEY, phone);
        editor.putString(ConstantManager.PROFILE_AVATAR_KEY, avatar);
        editor.apply();
    }

    public Map<String, String> loadUserProfileInfo() {
        Map<String, String> userProfileInfo = new HashMap<>();
        userProfileInfo.put(ConstantManager.PROFILE_FULL_NAME_KEY, mSharedPreferences.getString(ConstantManager.PROFILE_FULL_NAME_KEY, "USER NAME"));
        userProfileInfo.put(ConstantManager.PROFILE_PHONE_KEY, mSharedPreferences.getString(ConstantManager.PROFILE_PHONE_KEY, "USER PHONE"));
        userProfileInfo.put(ConstantManager.PROFILE_AVATAR_KEY, mSharedPreferences.getString(ConstantManager.PROFILE_AVATAR_KEY,
                ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/" + R.drawable.empty_user_avatar));
        return userProfileInfo;
    }

    public void removeUserAddress(int addressId) {
        if (addressId < 0) return;
        String strId = "_" + String.valueOf(addressId);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(ConstantManager.ADDRESS_NAME + strId);
        editor.remove(ConstantManager.ADDRESS_STREET + strId);
        editor.remove(ConstantManager.ADDRESS_HOUSE + strId);
        editor.remove(ConstantManager.ADDRESS_APT + strId);
        editor.remove(ConstantManager.ADDRESS_FLOOR + strId);
        editor.remove(ConstantManager.ADDRESS_COMMENT + strId);
        editor.apply();
    }

    public ArrayList<UserAddressDto> loadUserAddress() {
        ArrayList<UserAddressDto> list = new ArrayList<>();
        String strId;
        //make search for all addresses (assumes that ADDRESS_NAME can't be empty)
        for (int i = 0; i < 16; i++) {
            if (mSharedPreferences.getString(ConstantManager.ADDRESS_NAME + "_" + String.valueOf(i), "").isEmpty())
                continue;
            strId = "_" + String.valueOf(i);
            list.add(new UserAddressDto(i,
                    mSharedPreferences.getString(ConstantManager.ADDRESS_NAME + strId, "ADDRESS NAME"),
                    mSharedPreferences.getString(ConstantManager.ADDRESS_STREET + strId, "ADDRESS STREET"),
                    mSharedPreferences.getString(ConstantManager.ADDRESS_HOUSE + strId, "0"),
                    mSharedPreferences.getString(ConstantManager.ADDRESS_APT + strId, "0"),
                    mSharedPreferences.getInt(ConstantManager.ADDRESS_FLOOR + strId, 0),
                    mSharedPreferences.getString(ConstantManager.ADDRESS_COMMENT + strId, "ADDRESS_COMMENT")
            ));
        }
        return list;
    }

    public void saveUserSetting(String notificationKey, boolean isChecked) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(notificationKey, isChecked);
        editor.apply();
    }

    public Map<String, Boolean> getUserSettings() {
        Map<String, Boolean> userSettings = new HashMap<>();
        userSettings.put(ConstantManager.NOTIFICATION_ORDER_KEY, mSharedPreferences.getBoolean(ConstantManager.NOTIFICATION_ORDER_KEY, true));
        userSettings.put(ConstantManager.NOTIFICATION_PROMO_KEY, mSharedPreferences.getBoolean(ConstantManager.NOTIFICATION_PROMO_KEY, true));
        return userSettings;
    }

    public void updateOrInsertAddress(@NonNull UserAddressDto userAddressDto) {
        String strId = "_0";
        //if we adding new address, make search for free index (assumes that ADDRESS_NAME can't be empty), else edit previous address
        if (userAddressDto.getId() < 0) {
            for (int i = 0; i < 16; i++)//max 16 addresses allowed
                if (mSharedPreferences.getString(ConstantManager.ADDRESS_NAME + "_" + String.valueOf(i), "").isEmpty()) {
                    strId = "_" + String.valueOf(i);
                    break;
                }
        } else
            strId = "_" + String.valueOf(userAddressDto.getId());

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.ADDRESS_NAME + strId, userAddressDto.getName());
        editor.putString(ConstantManager.ADDRESS_STREET + strId, userAddressDto.getStreet());
        editor.putString(ConstantManager.ADDRESS_HOUSE + strId, userAddressDto.getHouse());
        editor.putString(ConstantManager.ADDRESS_APT + strId, userAddressDto.getApartment());
        editor.putInt(ConstantManager.ADDRESS_FLOOR + strId, userAddressDto.getFloor());
        editor.putString(ConstantManager.ADDRESS_COMMENT + strId, userAddressDto.getComment());
        editor.apply();
    }
}
