package yuribtr.skillbranch.ru.mvpauth5.data.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import yuribtr.skillbranch.ru.mvpauth5.App;
import yuribtr.skillbranch.ru.mvpauth5.data.network.NetworkStatusChecker;
import yuribtr.skillbranch.ru.mvpauth5.data.network.RestService;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.components.DaggerDataManagerComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.components.DataManagerComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.LocalModule;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.NetworkModule;


public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    @Inject
    NetworkStatusChecker mNetworkStatusChecker;

    private List<ProductDto> mMockProductList;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);
        generateMockData();
    }

    public boolean isNetworkPresence() {
        return mNetworkStatusChecker.isNetworkAvailable();
    }


    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public ProductDto getProductById(int productId) {
        return mMockProductList.get(productId - 1);
    }

    public void updateProduct(ProductDto product) {
        for (int i = 0; i < mMockProductList.size(); i++) {
            if (product.getId() == mMockProductList.get(i).getId())
                mMockProductList.set(i, product);
        }
    }

    public List<ProductDto> getMockProductList() {
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Audi A5 Cabriolet", "http://i.infocar.ua/img/models/3676.jpg", "Audi A5 Cabriolet (Ауди А5 Кабриолет) - кабриолет класса «H1» с передним или полным приводом. Автомобиль построен на платформе MLB, разработанной специалистами Volkswagen Group. Мировая премьера рестайлинговой версии модели состояла в сентябре 2011 года на автосалоне во Франкфурте.", 41000, 1));
        mMockProductList.add(new ProductDto(2, "Chevrolet Camaro", "http://i.infocar.ua/img/models/3212.jpg", "Chevrolet Camaro (Шевроле Камаро) - купе класса «G1». Мировая премьера рестайлинговой версии пятого поколения модели состоялась на автосалоне в Нью-Йорке в марте 2013 года.", 70000, 1));
        mMockProductList.add(new ProductDto(3, "Honda Accord", "http://i.infocar.ua/img/models/4067.jpg", "Honda Accord (Хонда Аккорд) – переднеприводный седан класса «D». Дебют рестайлинговой версии девятого поколения модели состоялся на автосалоне в Москве в августе 2014 года.", 29400, 1));
        mMockProductList.add(new ProductDto(4, "Ford Mustang", "http://i.infocar.ua/img/models/4276.jpg", "Ford Mustang (Форд Мустанг) - заднеприводное купе класса «G1». Мировая премьера модели состоялась в декабре 2013 года Европейская версия дебютировала в марте 2014 года на автосалоне в Женеве.", 57000, 1));
        mMockProductList.add(new ProductDto(5, "Mercedes AMG GT (С190)", "http://i.infocar.ua/img/models/4187.jpg", "Mercedes-AMG GT (Мерседес-АМГ GT) – заднеприводное купе класса «G2». Мировая премьера модели состоялась на автосалоне в Париже в октябре 2014 года.", 140000, 1));
        mMockProductList.add(new ProductDto(6, "Volkswagen Amarok DoubleCab", "http://i.infocar.ua/img/models/4925.jpg", "Volkswagen Amarok DoubleCab (Фольксваген Амарок ДаблКаб) – пикап класса «К4». Дебют рестайлинговой версии модели состоялся на выставке коммерческого транспорта в Ганновере в конце сентября 2016 года.", 39000, 1));
        mMockProductList.add(new ProductDto(7, "Toyota Land Cruiser 200", "http://i.infocar.ua/img/models/4594.jpg", "Toyota Land Cruiser 200 (Тойота Ленд Крузер 200) - полноприводный внедорожник класса «К2». В августе 2015 года автомобиль подвергся очередному обновлению.", 79000, 1));
        mMockProductList.add(new ProductDto(8, "Nissan Patrol", "http://i.infocar.ua/img/models/3944.jpg", "Nissan Patrol (Ниссан Патрол) - рамный внедорожник класса «K3». Дебют рестайлинговой версии шестого поколения модели состоялся в ноябре 2013 года в Дубае.", 56000, 1));
        mMockProductList.add(new ProductDto(9, "Mitsubishi Pajero Wagon", "http://i.infocar.ua/img/models/4380.jpg", "Mitsubishi Pajero Wagon (Мицубиси Паджеро Вагон) – пяти- или семиместный полноприводный внедорожник класса «К2». Рестайлинговая версия четвертого поколения модели дебютировала в августе 2014 года.", 45000, 1));
        mMockProductList.add(new ProductDto(10, "Hyundai Equus", "http://i.infocar.ua/img/models/979.jpg", "В дизайне Hyundai Equus (Хюндай Экус) сочетаются классические линии кузова, эффектная оптика и массивная решетка радиатора, создавая цельный, запоминающийся образ солидного и вместе с тем современного, динамичного автомобиля. ", 118000, 1));
    }

    public boolean isAuthUser() {
        return getPreferencesManager().getAuthToken() != null;
    }

    public void saveAuthToken(String authToken) {
        getPreferencesManager().saveAuthToken(authToken);
    }

    public Map<String, String> getUserProfileInfo() {
        return mPreferencesManager.loadUserProfileInfo();
    }

    public Map<String, Boolean> getUserSettings() {
        return mPreferencesManager.getUserSettings();
    }

    public void saveSetting(String notificationKey, boolean isChecked) {
        mPreferencesManager.saveUserSetting(notificationKey, isChecked);
    }

    public void removeUserAddress(int addressId) {
        mPreferencesManager.removeUserAddress(addressId);
    }

    public void updateOrInsertAddress(UserAddressDto userAddressDto) {
        mPreferencesManager.updateOrInsertAddress(userAddressDto);
    }

    public ArrayList<UserAddressDto> getUserAddress() {
        return mPreferencesManager.loadUserAddress();
    }

    public void saveProfileInfo(String name, String phone, String avatar) {
        mPreferencesManager.saveUserProfileInfo(name, phone, avatar);
    }

}
