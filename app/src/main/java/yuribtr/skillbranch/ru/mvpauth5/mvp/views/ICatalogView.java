package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;

public interface ICatalogView extends IView {

    void showCatalogView(List<ProductDto> productsList);

    void updateProductCounter();
}
