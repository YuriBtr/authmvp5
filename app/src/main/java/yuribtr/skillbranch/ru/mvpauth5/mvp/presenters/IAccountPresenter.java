package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

public interface IAccountPresenter {
    void clickOnAddress();

    void switchViewState(boolean saveSettings);

    void takePhoto();

    void chooseCamera();

    void chooseGallery();

    void removeAddress(int position);

    void editAddress(int position);

}
