package yuribtr.skillbranch.ru.mvpauth5.utils;

public interface Injector {
    /**
     * Injects a target object using this component's object graph.
     *
     * @param target the target object
     */
    public void inject(Object target);
}
