package yuribtr.skillbranch.ru.mvpauth5.mvp.models;

import android.os.AsyncTask;

import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.IAuthNotifier;


public class AuthModel extends AbstractModel implements IAuthModel {
    //todo: needed inject of AuthPresenter ???
    IAuthNotifier mPresenter;

    public AuthModel(IAuthNotifier presenter) {
        mPresenter = presenter;
    }

    //todo: check if it is possible to create w/o IAuthNotifier
    public AuthModel() {
    }

    public boolean isAuthUser() {
        return mDataManager.isAuthUser();
    }

    private void saveAuthToken(String authToken) {
        mDataManager.saveAuthToken(authToken);
    }

    public void loginUser(String email, String password) {
        if (mDataManager.isNetworkPresence()) {

            class WaitSplash extends AsyncTask<Void, Void, Void> {
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.currentThread();
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                    saveAuthToken("authenticated");
                    //todo: needed inject of AuthPresenter here
                    mPresenter.onLoginSuccess();
                }
            }
            WaitSplash waitSplash = new WaitSplash();
            waitSplash.execute();
        } else //todo: needed inject of AuthPresenter here and string resource ID?
            mPresenter.onLoginError("no network");//(DataManager.getInstance().getContext().getString(R.string.error_network_failure));
    }
}
