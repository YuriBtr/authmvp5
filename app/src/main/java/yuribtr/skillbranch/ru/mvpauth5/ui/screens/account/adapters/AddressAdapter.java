package yuribtr.skillbranch.ru.mvpauth5.ui.screens.account.adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> {

    private Context mContext;
    private ArrayList<UserAddressDto> mUserAddresses = new ArrayList<>();
    private AddressViewHolder.CustomClickListener mCustomClickListener;

//
//    public AddressAdapter(ArrayList<UserAddressDto> userAddresses, AddressViewHolder.CustomClickListener customClickListener) {
//        mUserAddresses = userAddresses;
//        this.mCustomClickListener = customClickListener;
//    }

    public void addItem(UserAddressDto address){
        mUserAddresses.add(address);
        notifyDataSetChanged();
    }

    public void reloadAdapter() {
        mUserAddresses.clear();
        notifyDataSetChanged();
    }


    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.address_item, parent, false);
        return new AddressViewHolder(convertView, mCustomClickListener);
    }

    @Override
    public void onBindViewHolder(final AddressViewHolder holder, int position) {
        UserAddressDto address = mUserAddresses.get(position);
        holder.addressName.setHint(address.getName());

        String tmpStr = address.getStreet() + " " + address.getHouse();

        if (!address.getApartment().isEmpty()) tmpStr = tmpStr + " - " + address.getApartment();
        if (address.getFloor() > 0)
            tmpStr = tmpStr + ", " + address.getFloor() + " " + mContext.getString(R.string.floor);

        holder.address.setText(tmpStr);
        if (address.getComment().isEmpty()) holder.addressComment.setVisibility(View.GONE);
        else holder.addressComment.setText(address.getComment());
    }


    @Override
    public int getItemCount() {
        return mUserAddresses.size();
    }

    public static class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.address_name_til)
        TextInputLayout addressName;

        @BindView(R.id.address_et)
        EditText address;

        @BindView(R.id.address_comment_et)
        EditText addressComment;
        private CustomClickListener mListener;

        public AddressViewHolder(View itemView, CustomClickListener customClickListener) {
            super(itemView);
            this.mListener = customClickListener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onUserItemClickListener(getAdapterPosition());
            }
        }

        public interface CustomClickListener {
            void onUserItemClickListener(int position);
        }
    }
}