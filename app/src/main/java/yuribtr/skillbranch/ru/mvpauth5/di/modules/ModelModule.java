package yuribtr.skillbranch.ru.mvpauth5.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.DataManager;

@Module
public class ModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
