package yuribtr.skillbranch.ru.mvpauth5.mortar;

import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mortar.MortarScope;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;

public class ScreenScoper {
    private static final String TAG = "ScreenScoper";
    private static Map<String, MortarScope> sScopeMap = new HashMap<>();

    //it is possible to refactor this Class and put Context + key in getScreenScope (2 methods, 10 code lines, w/o reflection)
    public static MortarScope getScreenScope(AbstractScreen screen) {
        if (!sScopeMap.containsKey(screen.getScopeName())) {
            Log.e(TAG, "getScreenScope: create new scope");
            return createScreenScope(screen);
        } else {
            Log.e(TAG, "getScreenScope: has scope");
            return sScopeMap.get(screen.getScopeName());
        }
    }

    public static void registerScope(MortarScope scope) {
        sScopeMap.put(scope.getName(), scope);
    }

    private static void cleanScopeName() {
        Iterator<Map.Entry<String, MortarScope>> iterator = sScopeMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MortarScope> entry = iterator.next();
            if (entry.getValue().isDestroyed()) {
                iterator.remove();
            }
        }
    }

    public static void destroyScreenScope(String scopeName) {
        MortarScope mortarScope = sScopeMap.get(scopeName);
        mortarScope.destroy();
        cleanScopeName();
    }

    @Nullable
    private static String getParentScopeName(AbstractScreen screen) {
        try {
            String genericsName = ((Class) ((ParameterizedType) screen.getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getName();
            String parentScopeName = genericsName;
            if (parentScopeName.contains("$")) {
                parentScopeName = parentScopeName.substring(0, genericsName.indexOf("$"));
            }
            return parentScopeName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //here we create visibility scope for our screen
    private static MortarScope createScreenScope(AbstractScreen screen) {
        Log.e(TAG, "createScreenScope: " + screen.getScopeName());
        //finding parent scope name and gets instance of parent scope
        MortarScope parentScope = sScopeMap.get(getParentScopeName(screen));
        //inside createScreenComponent created Dagger component (with all objects, that will be injected)
        Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
        MortarScope newScope = parentScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, screenComponent)
                .build(screen.getScopeName());//setting new name for child scope
        registerScope(newScope);
        return newScope;
    }
}
