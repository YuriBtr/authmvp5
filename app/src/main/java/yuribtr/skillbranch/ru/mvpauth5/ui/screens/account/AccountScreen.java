package yuribtr.skillbranch.ru.mvpauth5.ui.screens.account;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;
import yuribtr.skillbranch.ru.mvpauth5.BuildConfig;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ActivityResultDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserInfoDto;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserSettingsDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.AccountScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth5.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.IAccountPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.SubscribePresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.address.AddressScreen;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.os.Environment.DIRECTORY_PICTURES;
import static java.text.DateFormat.MEDIUM;
import static yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager.FILE_NAME_EXT;
import static yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager.FILE_TYPE_IMAGE;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region================DI================
    @dagger.Module
    public class Module {

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }

    //endregion


    //region================Presenter================
    public class AccountPresenter extends SubscribePresenter<AccountView> implements IAccountPresenter {
        private static final String TAG = "AccountPresenter";

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;
        private Subscription mAddressSub;
        private Subscription mSettingSub;
        private File mPhotoFile;
        private Subscription mActivityResultSub;
        private Subscription mPermissionResultSub;
        private Subscription mUserInfoSub;

        //region================LifeCycle================

        //when presenter creates
        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            //notify RootPresenter that we switched screen
            mRootPresenter.initView(scope);
            subscribeOnActivityResult();
            subscribeOnPermissionResult();
        }

        //this called when presenter load view
        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView();
            }
            subscribeOnAddressObs();
            subscribeOnSettingsObs();
            subscribeOnUserInfoObs();
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            mAddressSub.unsubscribe();
            mSettingSub.unsubscribe();
            mUserInfoSub.unsubscribe();
        }

        //when presenter destroyed
        @Override
        protected void onExitScope() {
            mActivityResultSub.unsubscribe();
            mPermissionResultSub.unsubscribe();
            super.onExitScope();
        }

        //endregion

        //region================Subscription================

        private void subscribeOnAddressObs() {
            mAddressSub = subscribe(mAccountModel.getAddressObs(), new ViewSubscriber<UserAddressDto>() {
                @Override
                public void onNext(UserAddressDto addressDto) {
                    if (getView() != null) {
                        getView().getAdapter().addItem(addressDto);
                    }
                }
            });
        }

        private void updateListView() {
            getView().getAdapter().reloadAdapter();
            subscribeOnAddressObs();
        }

        private void subscribeOnSettingsObs() {
            mSettingSub = subscribe(mAccountModel.getUserSettingsObs(), new ViewSubscriber<UserSettingsDto>() {
                @Override
                public void onNext(UserSettingsDto userSettingsDto) {
                    if (getView() != null) {
                        getView().initSettings(userSettingsDto);
                    }
                }
            });
        }

        private void subscribeOnPermissionResult() {
            Observable<Integer> permissionResultObs = mRootPresenter.getPermissionResultObs();
            mPermissionResultSub = subscribe(permissionResultObs, new ViewSubscriber<Integer>() {
                @Override
                public void onNext(Integer integer) {
                    handlePermissionResult(integer);
                }
            });
        }

        private void handlePermissionResult(Integer requestCode){
            switch (requestCode) {
                case (ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE):
                    chooseGallery();
                    break;
                case (ConstantManager.REQUEST_PERMISSION_CAMERA):
                    chooseCamera();
                    break;
            }
        }

        private void subscribeOnActivityResult() {
            Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultDtoObs()
                    .filter((ActivityResultDto activityResultDto) -> activityResultDto.getResultCode() == RESULT_OK);
            mActivityResultSub = subscribe(activityResultObs, new ViewSubscriber<ActivityResultDto>() {
                @Override
                public void onNext(ActivityResultDto activityResultDto) {
                    handleActivityResult(activityResultDto);
                }
            });
        }

        private void handleActivityResult(ActivityResultDto activityResultDto) {
            if (activityResultDto.getResultCode() == RESULT_OK) {
                Observable.just(activityResultDto.getRequestCode())
                        .flatMap(new Func1<Integer, Observable<Uri>>() {
                                     @Override
                                     public Observable<Uri> call(Integer integer) {
                                         switch (integer) {
                                             case ConstantManager.REQUEST_PROFILE_PHOTO_PICKER:
                                                 return Observable.just(activityResultDto.getIntent().getData());
                                             case ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA:
                                                 return Observable.just(Uri.fromFile(mPhotoFile));
                                             default:
                                                 return Observable.never();
                                         }
                                     }
                                 }
                        ).subscribe(uri -> {
                    if (getView() != null && uri != null) {
                        getView().updateAvatarPhoto(uri);
                    }
                }, Throwable::printStackTrace);
            }
        }

        private void subscribeOnUserInfoObs() {
            mUserInfoSub = subscribe(mAccountModel.getUserInfoObs(), new ViewSubscriber<UserInfoDto>() {
                @Override
                public void onNext(UserInfoDto userInfoDto) {
                    if (getView() != null) {
                        getView().updateProfileInfo(userInfoDto);
                    }
                }
            });
        }

        //endregion

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen(null));
        }

        @Override
        public void switchViewState(boolean saveSettings) {
            if (getCustomState() == AccountView.EDIT_STATE && getView() != null && saveSettings) {
                mAccountModel.saveProfileInfo(getView().getUserProfileInfo());
            }
            if (getView() != null) {
                getView().changeState();
            }
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        //region================CAMERA================

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionAndRequestIfNotGranted(permissions,
                        ConstantManager.REQUEST_PERMISSION_CAMERA)) {
                    mPhotoFile = createFileForPhoto();
                    if (mPhotoFile == null) {
                        getRootView().showMessage(R.string.photo_cannot_be_created);
                        return;
                    }
                    takePhotoFromCamera();
                }
            }
        }

        private void takePhotoFromCamera() {
            Uri uriForFile = FileProvider.getUriForFile(((RootActivity) getRootView()), BuildConfig.APPLICATION_ID + ".fileprovider", mPhotoFile);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
            ((RootActivity) getRootView()).startActivityForResult(takePictureIntent, ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
        }

        private File createFileForPhoto() {
            DateFormat dateTimeInstance = SimpleDateFormat.getTimeInstance(MEDIUM);
            String timeStamp = dateTimeInstance.format(new Date());
            String imageFileName = ConstantManager.PHOTO_FILE_PREFIX + timeStamp;
            File storageDir = getView().getContext().getExternalFilesDir(DIRECTORY_PICTURES);
            File fileImage;
            try {
                fileImage = File.createTempFile(imageFileName, FILE_NAME_EXT, storageDir);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return fileImage;
        }

        //endregion

        //region================GALLERY================

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                String[] permissions = new String[]{READ_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionAndRequestIfNotGranted(permissions,
                        ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {
                    takePhotoFromGallery();
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT < 19) {
                intent.setType(FILE_TYPE_IMAGE);
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setType(FILE_TYPE_IMAGE);
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            ((RootActivity) getRootView()).startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_PICKER);
        }

        //endregion

        @Override
        public void removeAddress(int position) {
            mAccountModel.removeAddress(mAccountModel.getAddressFromPosition(position));
            updateListView();
        }

        @Override
        public void editAddress(int position) {
            Flow.get(getView()).set(new AddressScreen(mAccountModel.getAddressFromPosition(position)));
        }

        @Nullable
        @Override
        protected IRootView getRootView() {
            return mRootPresenter.getView();
        }
        //endregion

        public void switchSettings() {
            if (getView() != null) {
                mAccountModel.saveSettings(getView().getSettings());
            }

        }
    }

}
