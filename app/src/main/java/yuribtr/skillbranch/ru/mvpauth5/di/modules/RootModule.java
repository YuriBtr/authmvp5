package yuribtr.skillbranch.ru.mvpauth5.di.modules;

import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.RootScope;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.RootPresenter;

//сначала создается модуль
@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }

    @Provides
    @RootScope
    AccountModel provideAccountModel() {
        return new AccountModel();
    }

}
