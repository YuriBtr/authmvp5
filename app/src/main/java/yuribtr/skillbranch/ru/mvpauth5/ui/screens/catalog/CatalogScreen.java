package yuribtr.skillbranch.ru.mvpauth5.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.CatalogScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth5.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.CatalogModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.ICatalogPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.auth.AuthScreen;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.product.ProductScreen;

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    //region================DI================
    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }

    }

    //CatalogPresenter наследуется от RootActivity.RootComponent
    //RootActivity.RootComponent инжектит  RootPresenter в RootActivity
    //чтобы из CatalogPresenter получить RootPresenter указываем в CatalogPresenter.RootComponent что он наследован от RootActivity.RootComponent
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        Picasso getPicasso();
    }

    //endregion

    //region================Presenter================
    public class CatalogPresenter extends ViewPresenter<CatalogView> implements ICatalogPresenter {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        CatalogModel mCatalogModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            mRootPresenter.initView(scope);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showCatalogView(mCatalogModel.getProductList());
            }
        }

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() != null) {
                if (checkUserAuth() && getRootView() != null) {
                    //todo:getString(R.string.goods)+mProductDtoList.get(position).getProductName()+getString(R.string.successfully_added_to_cart
                    getRootView().showMessage("Товар " + mCatalogModel.getProductList().get(position).getProductName() + " успешно добавлен в корзину");
                    //getView().showAddToCartMessage(mProductDtoList.get(position));
                    getView().updateProductCounter();
                } else {
                    Flow.get(getView()).set(new AuthScreen());//here we calling AuthScreen show
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAuth();
        }
    }
    //endregion

    public static class Factory {
        public static Context createProductContext(ProductDto product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope = null;
            ProductScreen screen = new ProductScreen(product);
            //here we generate different Scopenames for different products
            String scopeName = String.format("%s_%d", screen.getScopeName(), product.getId());
            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
