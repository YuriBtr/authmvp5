package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

public interface IAuthView extends IView {

    void showLoginBtn();

    void hideLoginBtn();

    void showCatalogScreen();

    String getEmail();

    String getPassword();

    boolean isIdle();

    void setCustomState(int state);
}
