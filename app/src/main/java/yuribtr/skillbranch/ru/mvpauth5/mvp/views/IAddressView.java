package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserAddressDto;

public interface IAddressView extends IView {

    void showInputError();

    void showMessage(int messageResId);

    UserAddressDto getUserAddress();
}
