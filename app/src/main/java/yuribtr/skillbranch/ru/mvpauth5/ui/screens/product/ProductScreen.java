package yuribtr.skillbranch.ru.mvpauth5.ui.screens.product;

import android.os.Bundle;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.ProductScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth5.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.CatalogModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.IProductPresenter;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.catalog.CatalogScreen;

@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {
    private ProductDto mProductDto;

    public ProductScreen(ProductDto product) {
        mProductDto = product;
    }

    @Override
    //we have to override this method in order to differ one product screen from another (as they have constructor with paremeters)
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductDto.equals(((ProductScreen) o).mProductDto);
    }

    @Override
    //we have to override this method in order to differ one product screen from another (as they have constructor with paremeters)
    public int hashCode() {
        return mProductDto.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);

        void inject(ProductView view);
    }

    //endregion

    //region================Presenter================

    public class ProductPresenter extends ViewPresenter<ProductView> implements IProductPresenter {

        @Inject
        CatalogModel mCatalogModel;
        private ProductDto mProduct;

        public ProductPresenter(ProductDto productDto) {
            mProduct = productDto;
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showProductView(mProduct);
            }
        }

        @Override
        public void clickOnPlus() {
            mProduct.addProduct();
            mCatalogModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }

        @Override
        public void clickOnMinus() {
            if (mProduct.getCount() > 1) {
                mProduct.deleteProduct();
                mCatalogModel.updateProduct(mProduct);
                if (getView() != null) {
                    getView().updateProductCountView(mProduct);
                }
            }
        }
    }
    //endregion


}
