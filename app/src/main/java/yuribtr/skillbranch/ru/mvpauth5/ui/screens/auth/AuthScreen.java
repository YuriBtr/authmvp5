package yuribtr.skillbranch.ru.mvpauth5.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.AuthScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth5.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AuthModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.IAuthPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth5.ui.activities.RootActivity;

//передаем родительский компонент, от которого этот экран будет зависеть (AuthScreen будет зависеть от RootActivity)
@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {
    private int mCustomState = 1;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }


    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
            //todo: check if it is possible to create w/o IAuthNotifier
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter presenter);//inject model instance to presenter

        void inject(AuthView view);//inject presenter to view

    }
    //endregion

    //region================Presenter================
    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {
        //ViewPresenter is Mortar class, which allow to save presenter in bundle
        //in generics we point which View connected to our presenter

        @Inject
        AuthModel mAuthModel;
        @Inject
        RootPresenter mRootPresenter;//this presenter needed for delegating it showMessage and etc

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            mRootPresenter.initView(scope);
        }

        //this method calling when View showed on screen
        @Override
        protected void onLoad(Bundle savedInstanceState) {
            //here will be all code for initializing View
            super.onLoad(savedInstanceState);
            //don't forget to check getView for null
            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(AuthView.LOGIN_STATE);
                } else {
                    boolean emailOk = getView().getEmail().matches(ConstantManager.PATTERN_EMAIL);
                    boolean passwordOk = getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD);
                    if (emailOk && passwordOk) {
                        getRootView().showMessage(getView().getContext().getString(R.string.user_authenticating_message));
                        //getRootView().showLoginProgress();
                        //mAuthModel.loginUser(getView().getEmail(), getView().getPassword());
                    } else {
                        //if (!emailOk) getView().setWrongEmailError();
                        //if (!passwordOk) getView().setWrongPasswordError();
                        getRootView().showMessage(getView().getContext().getString(R.string.email_or_password_wrong_format));
                    }
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnFb");
            }
        }

        @Override
        public void clickOnVk() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnVk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnTwitter");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage("Показать каталог");
            }
        }

        @Override
        public void onPasswordChanged() {

        }

        @Override
        public void onEmailChanged() {

        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }
    }

    //endregion
}
