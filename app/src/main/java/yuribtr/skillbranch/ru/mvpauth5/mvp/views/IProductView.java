package yuribtr.skillbranch.ru.mvpauth5.mvp.views;

import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;

public interface IProductView extends IView {
    void showProductView(ProductDto product);

    void updateProductCountView(ProductDto product);

}
