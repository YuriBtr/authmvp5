package yuribtr.skillbranch.ru.mvpauth5.ui.activities;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import yuribtr.skillbranch.ru.mvpauth5.BuildConfig;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.UserInfoDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.di.components.AppComponent;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.PicassoCacheModule;
import yuribtr.skillbranch.ru.mvpauth5.di.modules.RootModule;
import yuribtr.skillbranch.ru.mvpauth5.di.scopes.RootScope;
import yuribtr.skillbranch.ru.mvpauth5.flow.TreeKeyDispatcher;
import yuribtr.skillbranch.ru.mvpauth5.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth5.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IView;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.account.AccountScreen;
import yuribtr.skillbranch.ru.mvpauth5.ui.screens.catalog.CatalogScreen;
import yuribtr.skillbranch.ru.mvpauth5.utils.CircleTransform;

public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {
    //так как у этой активити пока нет презентера, хранить счетчик негде, поэтому и статическая переменная (позже будет переделано, с появлением презентера)
    private static int sGoodsCounter = 0;
    Menu mMenu;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorContainer;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    ImageView mCartBadgeIcon;
    TextView mCartBadgeText;

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                //todo:change it to CatalogScreen
                .defaultKey(new AccountScreen())//here default key will be CatalogScreen
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {//this method will take RootActivityScope and will find in AppContext scope RootActivity
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);
        initToolbar();
        mRootPresenter.takeView(this);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }
    //todo:show icon "back" at action bar


    public void setCartQuantity(int quantity) {
        sGoodsCounter = sGoodsCounter + quantity;
    }

    public void updateCartQuantity() {
        if (sGoodsCounter > 0) {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_black_24dp);
            mCartBadgeText.setText(String.valueOf(sGoodsCounter));
            mCartBadgeText.setVisibility(View.VISIBLE);
        } else {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_grey_24dp);
            mCartBadgeText.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.badge_menu, mMenu);
        MenuItem item = mMenu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.cart_badge);
        LinearLayout relativeLayout = (LinearLayout) MenuItemCompat.getActionView(item);
        mCartBadgeIcon = (ImageView) relativeLayout.findViewById(R.id.cart_icon_iv);
        mCartBadgeText = (TextView) relativeLayout.findViewById(R.id.cart_count_tv);
        setCartQuantity(0);
        updateCartQuantity();
        return super.onCreateOptionsMenu(menu);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == ConstantManager.CHECK_EXIT) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.check_exit_title);
            builder.setMessage(R.string.check_exit_question);
            builder.setIcon(android.R.drawable.ic_dialog_info);
            builder.setPositiveButton(R.string.yes, myClickListener);
            builder.setNegativeButton(R.string.no, myClickListener);
            return builder.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    moveTaskToBack(true);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }
        if (key != null) {
            Flow.get(this).set(key);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    //region================IRootView================
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int messageId) {
        Toast.makeText(this, getString(messageId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.unknown_error));
            //todo:send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void setMenuItemChecked(String name) {
        if (name.contains("AccountScreen")) mNavigationView.getMenu().getItem(0).setChecked(true);
        else if (name.contains("CatalogScreen"))
            mNavigationView.getMenu().getItem(1).setChecked(true);
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {
        View header = mNavigationView.getHeaderView(0);
        ImageView avatar = (ImageView) header.findViewById(R.id.drawer_user_avatar_iv);
        TextView username = (TextView) header.findViewById(R.id.drawer_user_name_tv);

        mPicasso.load(userInfoDto.getAvatar())
                .fit()
                .centerCrop()
                .transform(new CircleTransform())
                .into(avatar);
        username.setText(userInfoDto.getName());
    }

    public void openApplicationSettings() {
        //открываем настройки нашего приложения в системе при отсутствии разрешений и запрете на показ запроса
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, ConstantManager.PERMISSION_REQUEST_SETTINGS_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        showMessage("Moved to position: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    //endregion

    //region================DI================

    //затем создается компонент
    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    //берем RootPresenter и инжектим в RootActivity
    @RootScope    //RootActivity.RootComponent инжектит  RootPresenter в RootActivity
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(RootPresenter presenter);

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();//явно указываем что имеем доступ к RootPresenter из дочерних компонентов

        Picasso getPicasso();//for all child components will be available Picasso
        //чтобы из CatalogPresenter получить RootPresenter указываем в CatalogPresenter.RootComponent что он наследован от RootActivity.RootComponent
    }

    //endregion


}