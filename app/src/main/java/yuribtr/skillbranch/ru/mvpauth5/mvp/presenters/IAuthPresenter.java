package yuribtr.skillbranch.ru.mvpauth5.mvp.presenters;

public interface IAuthPresenter {

    void clickOnLogin();

    void clickOnFb();

    void clickOnVk();

    void clickOnTwitter();

    void clickOnShowCatalog();

    void onPasswordChanged();

    void onEmailChanged();

    boolean checkUserAuth();


}
