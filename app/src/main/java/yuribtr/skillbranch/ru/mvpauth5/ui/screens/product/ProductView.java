package yuribtr.skillbranch.ru.mvpauth5.ui.screens.product;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import yuribtr.skillbranch.ru.mvpauth5.R;
import yuribtr.skillbranch.ru.mvpauth5.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth5.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth5.mvp.views.IProductView;
import yuribtr.skillbranch.ru.mvpauth5.ui.custom_views.AspectRatioImageView;

public class ProductView extends LinearLayout implements IProductView {
    private static final String TAG = "ProductView";

    @BindView(R.id.product_name_tv)
    TextView mProductNameTxt;
    @BindView(R.id.product_description_tv)
    TextView mProductDescriptionTxt;
    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;
    @BindView(R.id.product_count_tv)
    TextView mProductCountTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;
    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @BindView(R.id.block_3)
    LinearLayout mBlock3;

    @Inject
    Picasso mPicasso;


    @Inject
    ProductScreen.ProductPresenter mPresenter;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region================LifeCycle================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region================IProductView================
    @Override
    public void showProductView(final ProductDto product) {
        mProductNameTxt.setText(product.getProductName());
        mProductDescriptionTxt.setText(product.getDescription());
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0)
            mProductPriceTxt.setText(adoptPrice(product.getCount() * product.getPrice()));
        else
            mProductPriceTxt.setText(adoptPrice(product.getPrice()));

        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) getContext().getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        int width, height;
        if (metrics.widthPixels < metrics.heightPixels) {
            width = (int) (metrics.widthPixels / 2f);
            height = (int) (width / AspectRatioImageView.getDefaultAspectRatio());
        } else {
            height = (int) (metrics.heightPixels / 5f);
            width = (int) (height * AspectRatioImageView.getDefaultAspectRatio());
        }
        mPicasso.setIndicatorsEnabled(true);
        mPicasso.load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .resize(width, height)
                .placeholder(R.drawable.no_image)
                .centerCrop()
                .into(mProductImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .fit()
                                .centerCrop()
                                .into(mProductImage);
                    }
                });
    }

    private String adoptPrice(int price) {
        return "$" + String.format(new Locale("ru"), "%,d", price) + ".-";
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0)
            mProductPriceTxt.setText(adoptPrice(product.getCount() * product.getPrice()));
        else
            mProductPriceTxt.setText(adoptPrice(product.getPrice()));
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    //endregion

    //region================Events================
    @OnClick(R.id.plus_btn)
    void clickPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickMinus() {
        mPresenter.clickOnMinus();
    }

    //endregion
}
